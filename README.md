# Lab5 -- Integration testing

## BVA

Type: "budget", "luxury", "nonsense"

Plan: "minute", "fixed_price", "nonsense"

Distance: "-10", "0", "10", "1000", "nonsense"

Planed Distance: "-10", "0", "10", "1000", "nonsense"

Time: "-10", "0", "10", "1000", "nonsense"

Planed time: "-10", "0", "10", "1000", "nonsense"

InnoDiscount: "yes", "no", "nonsense"


## Decision Table

| type | plan | distance | planed distance | time | planed time | planed distance | inno discount | expected | actual |
| - | - | - | - | - | - | - | - | - | - |
| budget | minute | 100 | 100 | 100 | 100 | 100 | yes | 1944 | {"price":1944.0000000000002} |
| budget | fixed_price | 100 | 100 | 100 | 100 | 100 | yes | 891 | {"price":1012.5000000000001} |
| budget | nonsense | 100 | 100 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request |
| luxury | minute | 100 | 100 | 100 | 100 | 100 | yes | 4779 | {"price":4779} |
| luxury | fixed_price | 100 | 100 | 100 | 100 | 100 | yes | Invalid Request | {"price":1012.5000000000001} |
| luxury | nonsense | 100 | 100 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request |
| nonsense | minute | 100 | 100 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request |
| nonsense | fixed_price | 100 | 100 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request |
| nonsense | nonsense | 100 | 100 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request |
| luxury | minute | -10 | -10 | 100 | 100 | -10 | yes | Invalid Request | Invalid Request |
| luxury | minute | -10 | 0 | 100 | 100 | 0 | yes | Invalid Request | Invalid Request |
| luxury | minute | -10 | 10 | 100 | 100 | 10 | yes | Invalid Request | Invalid Request |
| luxury | minute | -10 | 1000 | 100 | 100 | 1000 | yes | Invalid Request | Invalid Request |
| luxury | minute | -10 | nonsense | 100 | 100 | nonsense | yes | Invalid Request | Invalid Request |
| luxury | minute | 0 | -10 | 100 | 100 | -10 | yes | Invalid Request | Invalid Request |
| luxury | minute | 0 | 0 | 100 | 100 | 0 | yes | Invalid Request | {"price":4779} |
| luxury | minute | 0 | 10 | 100 | 100 | 10 | yes | Invalid Request | {"price":4779} |
| luxury | minute | 0 | 1000 | 100 | 100 | 1000 | yes | Invalid Request | {"price":4779} |
| luxury | minute | 0 | nonsense | 100 | 100 | nonsense | yes | Invalid Request | {"price":4779} |
| luxury | minute | 10 | -10 | 100 | 100 | -10 | yes | Invalid Request | Invalid Request |
| luxury | minute | 10 | 0 | 100 | 100 | 0 | yes | Invalid Request | {"price":4779} |
| luxury | minute | 10 | 10 | 100 | 100 | 10 | yes | 4779 | {"price":4779} |
| luxury | minute | 10 | 1000 | 100 | 100 | 1000 | yes | 4779 | {"price":4779} |
| luxury | minute | 10 | nonsense | 100 | 100 | nonsense | yes | Invalid Request | {"price":4779} |
| luxury | minute | 1000 | -10 | 100 | 100 | -10 | yes | Invalid Request | Invalid Request |
| luxury | minute | 1000 | 0 | 100 | 100 | 0 | yes | Invalid Request | {"price":4779} |
| luxury | minute | 1000 | 10 | 100 | 100 | 10 | yes | 4779 | {"price":4779} |
| luxury | minute | 1000 | 1000 | 100 | 100 | 1000 | yes | 4779 | {"price":4779} |
| luxury | minute | 1000 | nonsense | 100 | 100 | nonsense | yes | Invalid Request | {"price":4779} |
| luxury | minute | nonsense | -10 | 100 | 100 | -10 | yes | Invalid Request | Invalid Request |
| luxury | minute | nonsense | 0 | 100 | 100 | 0 | yes | Invalid Request | {"price":4779} |
| luxury | minute | nonsense | 10 | 100 | 100 | 10 | yes | Invalid Request | {"price":4779} |
| luxury | minute | nonsense | 1000 | 100 | 100 | 1000 | yes | Invalid Request | {"price":4779} |
| luxury | minute | nonsense | nonsense | 100 | 100 | nonsense | yes | Invalid Request | {"price":4779} |
| luxury | minute | 100 | 100 | -10 | -10 | 100 | yes | Invalid Request | Invalid Request |
| luxury | minute | 100 | 100 | -10 | 0 | 100 | yes | Invalid Request | Invalid Request |
| luxury | minute | 100 | 100 | -10 | 10 | 100 | yes | Invalid Request | Invalid Request |
| luxury | minute | 100 | 100 | -10 | 1000 | 100 | yes | Invalid Request | Invalid Request |
| luxury | minute | 100 | 100 | -10 | nonsense | 100 | yes | Invalid Request | Invalid Request |
| luxury | minute | 100 | 100 | 0 | -10 | 100 | yes | Invalid Request | Invalid Request |
| luxury | minute | 100 | 100 | 0 | 0 | 100 | yes | Invalid Request | {"price":0} |
| luxury | minute | 100 | 100 | 0 | 10 | 100 | yes | Invalid Request | {"price":0} |
| luxury | minute | 100 | 100 | 0 | 1000 | 100 | yes | Invalid Request | {"price":0} |
| luxury | minute | 100 | 100 | 0 | nonsense | 100 | yes | Invalid Request | {"price":0} |
| luxury | minute | 100 | 100 | 10 | -10 | 100 | yes | Invalid Request | Invalid Request |
| luxury | minute | 100 | 100 | 10 | 0 | 100 | yes | Invalid Request | {"price":477.90000000000003} |
| luxury | minute | 100 | 100 | 10 | 10 | 100 | yes | 477.9 | {"price":477.90000000000003} |
| luxury | minute | 100 | 100 | 10 | 1000 | 100 | yes | 477.9 | {"price":477.90000000000003} |
| luxury | minute | 100 | 100 | 10 | nonsense | 100 | yes | Invalid Request | {"price":477.90000000000003} |
| luxury | minute | 100 | 100 | 1000 | -10 | 100 | yes | Invalid Request | Invalid Request |
| luxury | minute | 100 | 100 | 1000 | 0 | 100 | yes | Invalid Request | {"price":47790} |
| luxury | minute | 100 | 100 | 1000 | 10 | 100 | yes | 47790 | {"price":47790} |
| luxury | minute | 100 | 100 | 1000 | 1000 | 100 | yes | 47790 | {"price":47790} |
| luxury | minute | 100 | 100 | 1000 | nonsense | 100 | yes | Invalid Request | {"price":47790} |
| luxury | minute | 100 | 100 | nonsense | -10 | 100 | yes | Invalid Request | Invalid Request |
| luxury | minute | 100 | 100 | nonsense | 0 | 100 | yes | Invalid Request | {"price":null} |
| luxury | minute | 100 | 100 | nonsense | 10 | 100 | yes | Invalid Request | {"price":null} |
| luxury | minute | 100 | 100 | nonsense | 1000 | 100 | yes | Invalid Request | {"price":null} |
| luxury | minute | 100 | 100 | nonsense | nonsense | 100 | yes | Invalid Request | {"price":null} |
| luxury | minute | 100 | 100 | 100 | 100 | 100 | yes | 4779 | {"price":4779} |
| luxury | minute | 100 | 100 | 100 | 100 | 100 | no | 5900 | {"price":5900} |
| luxury | minute | 100 | 100 | 100 | 100 | 100 | nonsense | Invalid Request | Invalid Request |

## Bugs

- Price for budget type with minute plan is not calculated correctly.
- Fixed price plan should not be allowed for luxury type.
- Distance, planed distance, time, planed time, planed distance should accept only numbers greater than zero.
